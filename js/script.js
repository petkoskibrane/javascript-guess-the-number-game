
let input = document.getElementById('userNumber');

let generatedRandomNumber = getRandomNumber();

let resultContainer = document.getElementById('resultContainer');

let tryAgainButton = document.getElementById('tryAgainButton');

let newGameButton = document.getElementById('newGameButton');

tryAgainButton.style.display = 'none';
newGameButton.style.display = 'none';


newGameButton.addEventListener('click',tryagain);
tryAgainButton.addEventListener('click',tryagain);

let counter = 10 ;

let counterSpan = document.getElementById('counter');
    counterSpan.innerHTML = counter.toString();

input.addEventListener('keydown',function (e) {
   if(e.keyCode === 13){
       checkNumber();
   }
});
    // ***********************************
    //     checkNumber main function
    // ***********************************
function checkNumber() {
    let userNumber = generateArrayOfDigits(input.value);
    let randomNumber = generateArrayOfDigits(generatedRandomNumber.toString());
    input.value = "";
    if(counter>1){
        counter--;
        counterSpan.innerHTML = counter;
        checkInPlace(userNumber,randomNumber);
    }else {
        //display try again
        resultContainer.innerHTML = "";
        tryAgainButton.style.display = 'block';
        input.style.display = 'none';


    }




}
// ***********************************
//     checkNumber main function end
// ***********************************


// ***********************************
//     checkNumber in place function
// ***********************************
function checkInPlace(userNumber,randomNumber) {
    for (let userNumIndex=0; userNumIndex < userNumber.length; userNumIndex++){

        for (let randomNumIndex=0; randomNumIndex < randomNumber.length; randomNumIndex++){

                if(userNumber[userNumIndex].number === randomNumber[randomNumIndex].number && (userNumIndex === randomNumIndex)){
                    
                    userNumber[userNumIndex].color = 'green';
                    userNumber[userNumIndex].inPlace = true;
                    randomNumber[randomNumIndex].color = 'green';
                    randomNumber[randomNumIndex].inPlace = true;


                }
        }

    }

    //after we checked digits in place we are checking guessed digits

    checkGuessedDigits(randomNumber,userNumber);

}

// ***************************************
//     checkNumber in place function end
// ***************************************

// ****************************************
//     checkNumber guessed digits function
// ****************************************
function checkGuessedDigits(randomNumber,userNumber) {
    for (var userNumIndex=0; userNumIndex < userNumber.length; userNumIndex++) {

        for (var randomNumIndex = 0; randomNumIndex < randomNumber.length; randomNumIndex++) {

            if(userNumber[userNumIndex].number === randomNumber[randomNumIndex].number){

                if(digitsAreNotInPlace() && digitsAreNotAlreadyChecked()){

                    console.log("yaeh2");
                    userNumber[userNumIndex].color = 'blue';
                    userNumber[userNumIndex].alreadyChecked = true;
                    randomNumber[randomNumIndex].color = 'blue';
                    randomNumber[randomNumIndex].alreadyChecked = true;
                }

            }

        }
    }



    displayResult(userNumber);




    function digitsAreNotAlreadyChecked() {
        return (userNumber[userNumIndex].alreadyChecked === false && randomNumber[randomNumIndex].alreadyChecked === false);


    }

    function digitsAreNotInPlace() {
        return (userNumber[userNumIndex].inPlace === false && randomNumber[randomNumIndex].inPlace === false);
    }




}

// **********************************************
//     checkNumber guessedDigits function end
// **********************************************


// *************************************************
//     generateArrayOfDigits function
//
//     we are generating array of digits objects
//
// ************************************************
function generateArrayOfDigits(number) {
    let arrayOfDigits = [];
    for (let i=0; i<number.length; i++){
        arrayOfDigits.push({
            number: number.charAt(i),
            color: 'grey',
            alreadyChecked : false,
            inPlace : false
        });
    }

    return arrayOfDigits;
}
// ****************************************
//     generateArrayOfDigits function end
// ****************************************




// ***********************************
//     getRandomNumber function
// ***********************************

function getRandomNumber() {

    // Math.floor(Math.random() * (max - min + 1)) + min;
    // The maximum is inclusive and the minimum is inclusive
    return Math.floor((Math.random()*9000)+1000);


}
// ***********************************
//     getRandomNumber function end
// ***********************************



// ***********************************
//     display result function
// ***********************************
function displayResult(userNumber) {
    let p = document.createElement('p');
    let guessed = 0;

    // we are creating span element with color for every digit


    for(let i = 0 ;i<userNumber.length;i++){
        let span = document.createElement('span');
        span.style.color = userNumber[i].color;
        span.innerHTML = userNumber[i].number;

        p.appendChild(span);
        if(userNumber[i].color === 'green') guessed++;
    }



    // if number is guessed we display new game button
    // and show Congratulations message

    if(guessed !== 4){
        resultContainer.appendChild(p);

    }else {
        resultContainer.innerHTML = "";
        resultContainer.appendChild(p);
        let p2 = document.createElement('p');
        p2.textContent = "Congratulations";
        resultContainer.appendChild(p2);
        input.style.display = 'none';
        newGameButton.style.display = 'block';
    }





}

// ***********************************
//     display result function end
// ***********************************


// ***************************************************
//     try again function
//
//     this function resets the game from the start
// ***************************************************
function tryagain() {
    counter = 10;
    counterSpan.innerHTML = counter;
    resultContainer.innerHTML = "";
    generatedRandomNumber = getRandomNumber();
    tryAgainButton.style.display = 'none';
    newGameButton.style.display = 'none';
    input.style.display = 'block';

}